package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Product;
import com.epam.rd.java.basic.task8.entity.Products;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	private final String xmlFileName;
	private static final String PRODUCT = "product";
	private static final String NAME = "name";
	private static final String TYPE = "type";
	private static final String VISUAL_PARAMETERS = "visualParameters";
	private static final String COLOUR = "colour";
	private static final String FORM = "form";
	private Products products;
	private StringBuilder elementValue;
	private VisualParameters visualParameters;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			if (elementValue == null) {
				elementValue = new StringBuilder();
			} else {
				elementValue.append(ch, start, length);
			}
		}

		@Override
		public void startDocument() throws SAXException {
			products = new Products();
		}

		@Override
		public void startElement(String uri, String lName, String qName, Attributes attr) throws SAXException {
			switch (qName) {
				case PRODUCT:
					this.products.getProductList().add(new Product());
					break;
				case VISUAL_PARAMETERS:
					this.latestProduct().setVisualParameters(new VisualParameters());
					break;
				default:
					elementValue = new StringBuilder();
					break;
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			switch (qName) {
				case NAME:
					latestProduct().setName(elementValue.toString());
					break;
				case TYPE:
					latestProduct().setType(elementValue.toString());
					break;
				case COLOUR:
					latestProduct().getVisualParameters().setColor(elementValue.toString());
					break;
				case FORM:
					latestProduct().getVisualParameters().setForm(elementValue.toString());
					break;
			}
		}

		private Product latestProduct() {
			List<Product> productList = this.products.getProductList();
			int latestProductIndex = productList.size() - 1;
			return productList.get(latestProductIndex);
		}

		public Products getProducts() {
			return this.products;
		}

		public static void sortByName(Products products) {
			products.getProductList().sort(Comparator.comparing(Product::getName));
		}

		public static void write(String outFile, Products products) {
			XMLOutputFactory xof = XMLOutputFactory.newInstance();
			XMLStreamWriter xsw = null;
			try {
				xsw = xof.createXMLStreamWriter(new FileWriter(outFile));
				xsw.writeStartDocument();
				xsw.writeStartElement("products");
				xsw.writeAttribute("xmlns", "http://www.nure.ua");

				for (Product prod : products.getProductList()) {
					xsw.writeStartElement("product");
					xsw.writeStartElement("name");
					xsw.writeCharacters(prod.getName());
					xsw.writeEndElement();

					xsw.writeStartElement("type");
					xsw.writeCharacters(prod.getType());
					xsw.writeEndElement();

					xsw.writeStartElement("visualParameters");
					xsw.writeStartElement("colour");
					xsw.writeCharacters(prod.getVisualParameters().getColor());
					xsw.writeEndElement();
					xsw.writeStartElement("form");
					xsw.writeCharacters(prod.getVisualParameters().getForm());
					xsw.writeEndElement();
					xsw.writeEndElement();
					xsw.writeEndElement();
				}

				xsw.writeEndElement();
				xsw.writeEndDocument();
				xsw.flush();
			} catch (Exception e) {
				System.err.println("Unable to write the file: " + e.getMessage());
			} finally {
				try {
					if (xsw != null) {
						xsw.close();
					}
				} catch (Exception e) {
					System.err.println("Unable to close the file: " + e.getMessage());
				}
			}
		}
}