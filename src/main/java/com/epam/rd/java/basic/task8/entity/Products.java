package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

public class Products {
    private List<Product> productList;

    public Products() {
        this.productList = new ArrayList<>();
    }

    public List<Product> getProductList() {
        return productList;
    }
}
