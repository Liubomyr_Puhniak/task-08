package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Product;
import com.epam.rd.java.basic.task8.entity.Products;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.*;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.Comparator;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
	private XMLEventReader reader;
	private Products products;

	public STAXController(String xmlFileName) throws FileNotFoundException, XMLStreamException {
		this.xmlFileName = xmlFileName;
		reader = xmlInputFactory.createXMLEventReader(new FileInputStream(this.xmlFileName));
		this.products = new Products();
	}

	public void parse() throws XMLStreamException {
		Product product = new Product();
		VisualParameters visualParameters = new VisualParameters();
		while (reader.hasNext()) {
			XMLEvent nextEvent = reader.nextEvent();
			if (nextEvent.isStartElement()) {
				StartElement startElement = nextEvent.asStartElement();
				switch (startElement.getName().getLocalPart()) {
					case "product":
						product = new Product();
						break;
					case "name":
						nextEvent = reader.nextEvent();
						product.setName(nextEvent.asCharacters().getData());
						break;
					case "type":
						nextEvent = reader.nextEvent();
						product.setType(nextEvent.asCharacters().getData());
						break;
					case "visualParameters":
						visualParameters = new VisualParameters();
						product.setVisualParameters(visualParameters);
						nextEvent = reader.nextEvent();
						nextEvent = reader.nextEvent();
						if(nextEvent.asStartElement().getName().getLocalPart().equals("colour")) {
							nextEvent = reader.nextEvent();
							product.getVisualParameters().setColor(nextEvent.asCharacters().getData());
							nextEvent = reader.nextEvent();
							nextEvent = reader.nextEvent();
							nextEvent = reader.nextEvent();
						}
						if(nextEvent.asStartElement().getName().getLocalPart().equals("form")) {
							nextEvent = reader.nextEvent();
							product.getVisualParameters().setForm(nextEvent.asCharacters().getData());
						}
						break;
				}
			}
			if (nextEvent.isEndElement()) {
				EndElement endElement = nextEvent.asEndElement();
				if (endElement.getName().getLocalPart().equals("product")) {
					products.getProductList().add(product);
				}
			}
		}
	}

	public Products getProducts() {
		return this.products;
	}

	public static void sortByName(Products products) {
		products.getProductList().sort(Comparator.comparing(Product::getName));
	}

	public static void writeXml(OutputStream out, Products products) throws XMLStreamException {

		XMLOutputFactory output = XMLOutputFactory.newInstance();

		XMLStreamWriter writer = output.createXMLStreamWriter(out);

		writer.writeStartDocument("utf-8", "1.0");

		writer.writeStartElement("products");

		writer.writeAttribute("xmlns", "http://www.nure.ua");

		for (Product prod: products.getProductList()) {
			writer.writeStartElement("product");
			writer.writeStartElement("name");
			writer.writeCharacters(prod.getName());
			writer.writeEndElement();
			writer.writeStartElement("type");
			writer.writeCharacters(prod.getType());
			writer.writeEndElement();
			writer.writeStartElement("visualParameters");
			writer.writeStartElement("colour");
			writer.writeCharacters(prod.getVisualParameters().getColor());
			writer.writeEndElement();
			writer.writeStartElement("form");
			writer.writeCharacters(prod.getVisualParameters().getForm());
			writer.writeEndElement();
			writer.writeEndElement();
			writer.writeEndElement();
		}

		writer.writeEndElement();

		writer.writeEndDocument();

		writer.flush();

		writer.close();

	}

}