package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {

    private String color;
    private String form;

    public VisualParameters(String color, String form) {
        this.color = color;
        this.form = form;
    }

    public VisualParameters() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }
}
