package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class Product {

    private String name;
    private String type;
    private VisualParameters visualParameters;

    public Product(String name, String type, VisualParameters visualParameters) {
        this.name = name;
        this.type = type;
        this.visualParameters = visualParameters;
    }

    public Product() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(name, product.name) && Objects.equals(type, product.type) && Objects.equals(visualParameters, product.visualParameters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, visualParameters);
    }
}
