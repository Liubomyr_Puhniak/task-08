package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.Product;
import com.epam.rd.java.basic.task8.entity.Products;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Comparator;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Products parse() {
		Products result = new Products();

		try {
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = documentBuilder.parse(xmlFileName);

			document.getDocumentElement().normalize();

			NodeList products = document.getElementsByTagName("product");

			for (int i = 0; i < products.getLength(); i++) {
				Node productNode = products.item(i);
				Product productClass = new Product();

				if (productNode.getNodeType() != Node.TEXT_NODE) {
					NodeList productProps = productNode.getChildNodes();

					for(int j = 0; j < productProps.getLength(); j++) {
						Node prodProp = productProps.item(j);

						if(prodProp.getNodeName().equals("name")) {
							productClass.setName(prodProp.getTextContent());
						} else if(prodProp.getNodeName().equals("type")) {
							productClass.setType(prodProp.getTextContent());
						}  else if(prodProp.getNodeName().equals("visualParameters")) {
							NodeList visualParamsNodes = prodProp.getChildNodes();
							VisualParameters visualParametersClass = new VisualParameters();

							for(int g = 0; g < visualParamsNodes.getLength(); g++) {
								Node visualParam = visualParamsNodes.item(g);

								if(visualParam.getNodeName().equals("colour")) {
									visualParametersClass.setColor(visualParam.getTextContent());
								} else if(visualParam.getNodeName().equals("form")) {
									visualParametersClass.setForm(visualParam.getTextContent());
								}

							}
							productClass.setVisualParameters(visualParametersClass);
						}

					}
					result.getProductList().add(productClass);
				}
			}

		} catch (ParserConfigurationException | SAXException | IOException ex) {
			ex.printStackTrace(System.out);
		}

		return result;
	}

	// Функция добавления новой книги и записи результата в файл
	public static void write(String fileName, String nameOut, Products products) {
		try{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element root = doc.createElement("products");
			root.setAttribute("xmlns", "http://www.nure.ua");
			doc.appendChild(root);

			for (Product prod: products.getProductList()) {
				Element product = doc.createElement("product");

				Element name = doc.createElement("name");

				name.setTextContent(prod.getName());

				Element type = doc.createElement("type");
				type.setTextContent(prod.getType());

				Element visualParams = doc.createElement("visualParameters");

				Element colour = doc.createElement("colour");
				colour.setTextContent(prod.getVisualParameters().getColor());

				Element form = doc.createElement("form");
				form.setTextContent(prod.getVisualParameters().getForm());

				visualParams.appendChild(colour);
				visualParams.appendChild(form);

				product.appendChild(name);
				product.appendChild(type);
				product.appendChild(visualParams);
				root.appendChild(product);
			}

			try (FileOutputStream output = new FileOutputStream(nameOut)) {
				writeDocument(doc, output);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (TransformerException | ParserConfigurationException e) {
			e.printStackTrace(System.out);
		}

	}

	// Функция для сохранения DOM в файл
	private static void writeDocument(Document document, FileOutputStream nameOut) throws TransformerFactoryConfigurationError, TransformerException, FileNotFoundException {
			Transformer tr = TransformerFactory.newInstance().newTransformer();
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(nameOut);
			tr.transform(source, result);

	}

	public static void sortByName(Products products) {
		products.getProductList().sort(Comparator.comparing(Product::getName));
	}

}
