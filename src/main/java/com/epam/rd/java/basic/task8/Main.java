package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Products;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileOutputStream;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		Products domProducts = domController.parse();
		// sort (case 1)
		DOMController.sortByName(domProducts);
		// save
		String outputXmlFile = "output.dom.xml";
		DOMController.write(xmlFileName, outputXmlFile, domProducts);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			File file = new File(xmlFileName);
			saxParser.parse(file, saxController);

			Products saxProducts = saxController.getProducts();

			// sort  (case 2)
			SAXController.sortByName(saxProducts);

			// save
			outputXmlFile = "output.sax.xml";
			SAXController.write(outputXmlFile, saxProducts);

		} catch (Exception e) {
			e.printStackTrace();
		}

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		Products staxproducts = staxController.getProducts();
		
		// sort  (case 3)
		STAXController.sortByName(staxproducts);
		
		// save
		outputXmlFile = "output.stax.xml";
		STAXController.writeXml(new FileOutputStream(outputXmlFile), staxproducts);
	}

}
